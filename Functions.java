import java.math.BigInteger;
import java.math.BigDecimal;
import java.math.RoundingMode;

class Functions
{
	public BigDecimal sin(BigDecimal x, int precision)
	{
		BigDecimal negativeOne = (BigDecimal.ZERO).subtract(BigDecimal.ONE).setScale(precision, RoundingMode.HALF_UP);
		
		BigDecimal termDenominator = BigDecimal.ONE.setScale(precision, RoundingMode.HALF_UP);
		BigDecimal factorialUp = BigDecimal.ONE.setScale(precision, RoundingMode.HALF_UP);
		
		BigDecimal termNumerator = x.add(BigDecimal.ZERO).setScale(precision, RoundingMode.HALF_UP);
		BigDecimal termSign = BigDecimal.ONE.setScale(precision, RoundingMode.HALF_UP);
		BigDecimal calculation = termNumerator.divide(termDenominator, precision, RoundingMode.HALF_UP).setScale(precision, RoundingMode.HALF_UP);
		
		for(int index = 0; index < precision; index++)
		{
			termSign = termSign.multiply(negativeOne).setScale(precision, RoundingMode.HALF_UP);
			
			factorialUp = factorialUp.add(BigDecimal.ONE).setScale(precision, RoundingMode.HALF_UP);
			termDenominator = termDenominator.multiply(factorialUp).setScale(precision, RoundingMode.HALF_UP);
			factorialUp = factorialUp.add(BigDecimal.ONE).setScale(precision, RoundingMode.HALF_UP);
			termDenominator = termDenominator.multiply(factorialUp).setScale(precision, RoundingMode.HALF_UP);
			
			termNumerator = termNumerator.multiply(x).setScale(precision, RoundingMode.HALF_UP).multiply(x).setScale(precision, RoundingMode.HALF_UP);
			
			calculation = calculation.add(termSign.multiply(termNumerator.divide(termDenominator, precision, RoundingMode.HALF_UP))).setScale(precision, RoundingMode.HALF_UP);
		}
		
		return calculation;
	}
	
	public BigDecimal cos(BigDecimal x, int precision)
	{
		BigDecimal negativeOne = (BigDecimal.ZERO).subtract(BigDecimal.ONE).setScale(precision, RoundingMode.HALF_UP);
		
		BigDecimal termDenominator = BigDecimal.ONE.setScale(precision, RoundingMode.HALF_UP);
		BigDecimal factorialUp = BigDecimal.ZERO.setScale(precision, RoundingMode.HALF_UP);
		
		BigDecimal termNumerator = BigDecimal.ONE.setScale(precision, RoundingMode.HALF_UP);
		BigDecimal termSign = BigDecimal.ONE.setScale(precision, RoundingMode.HALF_UP);
		BigDecimal calculation = termNumerator.divide(termDenominator, precision, RoundingMode.HALF_UP).setScale(precision, RoundingMode.HALF_UP);
		
		for(int index = 0; index < precision; index++)
		{
			termSign = termSign.multiply(negativeOne).setScale(precision, RoundingMode.HALF_UP);
			
			factorialUp = factorialUp.add(BigDecimal.ONE).setScale(precision, RoundingMode.HALF_UP);
			termDenominator = termDenominator.multiply(factorialUp).setScale(precision, RoundingMode.HALF_UP);
			factorialUp = factorialUp.add(BigDecimal.ONE).setScale(precision, RoundingMode.HALF_UP);
			termDenominator = termDenominator.multiply(factorialUp).setScale(precision, RoundingMode.HALF_UP);
			
			termNumerator = termNumerator.multiply(x).setScale(precision, RoundingMode.HALF_UP).multiply(x).setScale(precision, RoundingMode.HALF_UP);
			
			calculation = calculation.add(termSign.multiply(termNumerator.divide(termDenominator, precision, RoundingMode.HALF_UP))).setScale(precision, RoundingMode.HALF_UP);
		}
		
		return calculation;
	}
}